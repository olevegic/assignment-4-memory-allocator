#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
  *((struct block_header *)addr) = (struct block_header){
      .next = next,
      .capacity = capacity_from_size(block_sz),
      .is_free = true};
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags)
{
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static size_t get_region_size(size_t query)
{
  block_capacity q_capacity = (block_capacity){.bytes = query};
  block_size q_size = size_from_capacity(q_capacity);
  return q_size.bytes;
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query)
{
  size_t actual_size = region_actual_size(get_region_size(query));
  void *rg_addr = map_pages(addr, actual_size, MAP_FIXED | MAP_FIXED_NOREPLACE);
  bool extends = true;
  if (rg_addr == MAP_FAILED)
  {
    extends = false;
    rg_addr = map_pages(addr, actual_size, 0);
    if (rg_addr == MAP_FAILED)
    {
      return REGION_INVALID;
    }
  }

  block_init(rg_addr, (block_size){.bytes = actual_size}, NULL);
  return (struct region){.addr = rg_addr, .size = actual_size, .extends = extends};
}

static void *block_after(struct block_header const *block);

static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd);

void *heap_init(size_t initial)
{
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term()
{

  struct block_header *current_header = HEAP_START;

  while (current_header != NULL)
  {
    size_t current_region_size = 0;
    void *current_region_start = current_header;

    while (current_header->next == block_after(current_header))
    {
      current_region_size += size_from_capacity(current_header->capacity).bytes;
      current_header = current_header->next;
    }
    current_region_size += size_from_capacity(current_header->capacity).bytes;

    current_header = current_header->next;

    munmap(current_region_start, current_region_size);
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query)
{
  return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query)
{
  if (!block_splittable(block, query))
    return false;
  void *new_block_ptr = block->contents + query;
  block_size size = (block_size){.bytes = (block->capacity.bytes - query)};
  block_init(new_block_ptr, size, block->next);
  block->next = new_block_ptr;
  block->capacity.bytes = query;
  return true;
}
/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block)
{
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block)
{
  if (!block)
    return false;
  struct block_header *next_block = block->next;
  if (next_block && mergeable(block, next_block))
  {
    block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
    block_init(block, size_from_capacity(block->capacity), next_block->next);
    return true;
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result
{
  enum
  {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED
  } type;
  struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz)
{
  if (!block)
  {
    return (struct block_search_result){BSR_CORRUPTED};
  }
  struct block_header *previous = NULL;
  while (block)
  {
    if (try_merge_with_next(block))
    {
      continue;
    }
    if (block->is_free && block->capacity.bytes >= sz)
    {
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
    }
    previous = block;
    block = block->next;
  }
  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, previous};
}

static void try_merge_all_blocks(struct block_header *block)
{
  struct block_header *cur_block = block;
  if (!block)
    return;
  while (cur_block->next != NULL)
  {
    if (try_merge_with_next(cur_block))
    {
      continue;
    }
    cur_block = cur_block->next;
  }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
  struct block_search_result res = find_good_or_last(block, query);
  if (res.type != BSR_FOUND_GOOD_BLOCK)
  {
    return res;
  }
  split_if_too_big(res.block, query);
  res.block->is_free = false;
  return res;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query)
{
  if (!last)
  {
    return NULL;
  }
  struct region region = alloc_region(block_after(last), query);
  if (region_is_invalid(&region))
  {
    return NULL;
  }
  last->next = region.addr;
  if (try_merge_with_next(last))
  {
    return last;
  }
  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
  size_t adjusted_query = (query < BLOCK_MIN_CAPACITY) ? BLOCK_MIN_CAPACITY : query;

  struct block_search_result result = try_memalloc_existing(adjusted_query, heap_start);

  if (result.type == BSR_FOUND_GOOD_BLOCK)
  {
    return result.block;
  }

  if (result.type == BSR_REACHED_END_NOT_FOUND)
  {
    struct block_header *expanded = grow_heap(result.block, size_from_capacity((block_capacity){.bytes = adjusted_query}).bytes);
    if (!expanded)
    {
      return NULL;
    }

    result = try_memalloc_existing(adjusted_query, expanded);
    if (result.type == BSR_FOUND_GOOD_BLOCK)
    {
      return result.block;
    }
  }

  return NULL;
}

void *_malloc(size_t query)
{
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem)
{
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  try_merge_all_blocks(header);
}
